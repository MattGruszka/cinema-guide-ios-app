//
//  Film.swift
//  CinemaGuide
//
//  Created by Gruszkovsky on 10.05.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

struct Film {
    let name: String
    let backImage: UIImage
    let runtime: Int
    let tag: String
    let voteCount: Int
    let voteAverage: Double
    let releaseDate: String
    let overview: String
    let arrayScreens: [UIImage]
}
