//
//  FilmDetailViewController.swift
//  CinemaGuide
//
//  Created by Gruszkovsky on 05.05.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

class FilmDetailViewController: UIViewController{
    
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var starImageView: UIImageView!
    @IBOutlet weak var overview: UITextView!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var test: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var voteCount: UILabel!
    @IBOutlet weak var voteAv: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    
    var filmNextScene : Film? = nil
    var runtimeHolder: Int? = nil
    var tagHolder: String? = nil
    var voteCountHolder: Int? = nil
    var voteAverageHolder: Double? = nil
    var releaseDateHolder: String? = nil
    var overviewHolder: String? = nil
    var name: String? = nil
    var backImageHolder: UIImage? = nil
    var arrayScreensHolder: [UIImage]? = nil
    let quote = "\""
    let percent = "%"
    var starImage: UIImage? = nil


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back")
        
        
        runtimeHolder = filmNextScene?.runtime
        tagHolder = filmNextScene?.tag
        voteCountHolder = filmNextScene?.voteCount
        voteAverageHolder = filmNextScene?.voteAverage
        releaseDateHolder = filmNextScene?.releaseDate
        overviewHolder = filmNextScene?.overview
        name = filmNextScene?.name
        backImageHolder = filmNextScene?.backImage
        arrayScreensHolder = filmNextScene?.arrayScreens
        
        self.navigationItem.title = name
        time.text = "\(runtimeHolder!) min"
        tagline.text = quote + tagHolder! + quote
        overview.text = overviewHolder
        voteCount.text = "Vote count: " + String(voteCountHolder!)
        voteAv.text = String(Int(voteAverageHolder! * 10)) + percent
        releaseDate.text = "Release date: " + String(releaseDateHolder!)
        backImage.image = backImageHolder
        test.text = name
        starImageView.image = putStarImgage()
        secondImageView.image = arrayScreensHolder![1]

    }
    
    func putStarImgage() -> UIImage {
        if ((filmNextScene!.voteAverage * 10) < 10) {
            starImage = UIImage(named: "0star")
        } else if (((filmNextScene!.voteAverage * 10) < 20 && ((filmNextScene!.voteAverage * 10) >= 10))) {
            starImage = UIImage(named: "1star")
        } else if (((filmNextScene!.voteAverage * 10) < 40 && ((filmNextScene!.voteAverage * 10) >= 20))) {
            starImage = UIImage(named: "2star")
        } else if (((filmNextScene!.voteAverage * 10) < 70 && ((filmNextScene!.voteAverage * 10) >= 40))) {
            starImage = UIImage(named: "3star")
        } else if (((filmNextScene!.voteAverage * 10) < 90 && ((filmNextScene!.voteAverage * 10) >= 70))) {
            starImage = UIImage(named: "4star")
        } else {
            starImage = UIImage(named: "5star")
        }
        return starImage!
    }
}
