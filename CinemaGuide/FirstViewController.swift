//
//  FirstViewController.swift
//  CinemaGuide
//
//  Created by Gruszkovsky on 26.04.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//


import UIKit

class FirstViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    typealias JSONStandard = [String : Any]
    let key: String = "?api_key=747a58c92f7adee7f4b6e790a027a968"
    let base: String = "https://api.themoviedb.org/3/movie/"
    let upcoming: String = "upcoming"       //dziala
    let nowPlaying: String = "now_playing" //nie dziala
    let topRated: String = "top_rated" //nie dziala
    let popular: String = "popular" //dziala
    let vidAndImgCall: String = "&append_to_response=videos,images" // ??
    let imagePath: String = "https://image.tmdb.org/t/p/w500"  // dziala
    
    //url = base + tag + key
    
    var arrayWithImageUrls = [String]()
    var arrayWithImages = [UIImage]()
    var arrayWithID = [Int]()
    var INDEX_NUMBER_BEFORE = Int()
    var arrayWithFilms = [Film]()
    var nameOfFilm: String = "test"
    var film : Film? = nil
    
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var MyCollectionView: UICollectionView!
    
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        let nameOfSearch: String
        switch sender.selectedSegmentIndex {
        case 0: nameOfSearch = "now_playing"; print("NOW PLAYING FIRST")
        case 1: nameOfSearch = "upcoming"; print("UPCOMING SECOND")
        case 2: nameOfSearch = "top_rated"; print("TOP RATED THIRD")
        case 3: nameOfSearch = "popular"; print("POPULAR FOURTH")
        default: nameOfSearch = ""
        }
        print("now im downloading JSON with\(nameOfSearch)")
        downloadJson(url: "https://api.themoviedb.org/3/movie/\(nameOfSearch)\(key)&append_to_response=videos,images", completion: { newArrayID in
            self.arrayWithID = newArrayID})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadJson(url: "https://api.themoviedb.org/3/movie/now_playing\(key)&append_to_response=videos,images",
            completion: { newArrayID in
                self.arrayWithID = newArrayID
                self.goDispatch()
        })
        self.MyCollectionView.delegate = self
        self.MyCollectionView.dataSource = self
        
    }
    
    func goDispatch(){
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                for i in self.arrayWithID {
                    self.singleRequest(id: i, completion: { f in
                        let film = f
                        self.arrayWithFilms.append(film)
                    })
                }
            }
        }
    
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayWithImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        INDEX_NUMBER_BEFORE = indexPath.row
        
        let nextScene = storyboard?.instantiateViewController(withIdentifier: "FilmDetailViewController") as! FilmDetailViewController
            let f = arrayWithFilms[INDEX_NUMBER_BEFORE]
            nextScene.filmNextScene = f
            self.navigationController?.pushViewController(nextScene, animated: true)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->
        UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collection_cell", for: indexPath)
            as! MyCollectionViewCell
            cell.myImageView.image = arrayWithImages[indexPath.row]
            return cell
    }
    
    func downloadJson(url: String, completion: @escaping ([Int]) -> Void) {
        arrayWithFilms.removeAll()
        arrayWithImages.removeAll()
        arrayWithID.removeAll()
        var newArrayID = [Int]()
                let request = URLRequest(url: URL(string: url)!)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard let data = data else {
                print("failed")
                return
            }
            
                do {
                    var readableJSON = try JSONSerialization.jsonObject(with: data) as! JSONStandard
                    if let results = readableJSON["results"] as? [JSONStandard]{
                        for i in 0..<results.count{
                            let item = results[i]
                            let picPath = item["poster_path"] as! String
                            let title = item ["original_title"] as! String
                            let id = item["id"] as! Int
                            print("\(title) - \(id)")

                            self.arrayWithImageUrls.append(self.imagePath + picPath)
                            let myImageURL = URL(string: (self.imagePath+picPath))
                            let myImageData = NSData(contentsOf: myImageURL!)
                            let myImage = UIImage(data: myImageData! as Data)
                            self.arrayWithImages.append(myImage!)
                            newArrayID.append(id)
                            self.MyCollectionView.reloadData()
                        }
                        completion(newArrayID)
                        print(newArrayID)
                    }
                } catch {
                    print("Error")
                }

            }
        
        task.resume()
       
    }
    
    func singleRequest(id: Int, completion: @escaping (Film) -> Void ){
        let request = URLRequest(url: URL(string: "https://api.themoviedb.org/3/movie/\(id)\(key)&append_to_response=videos,images")!)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard let data = data else {
                print("failed")
                return
            }
            do {
                var arrayWithSomeScreens = [UIImage]()
                var readableJSON = try JSONSerialization.jsonObject(with: data) as! [String : Any]
                let name = readableJSON["original_title"] as! String
                let backImage = readableJSON["backdrop_path"] as! String
                let imagesJSON = readableJSON["images"] as! [String : Any]
                let arrayImagesJSON = imagesJSON["backdrops"] as! [[String: Any]]
                for i in arrayImagesJSON {
                    //pobrac tylko 2 fotki a nie wszystkie
                    let image = i["file_path"] as! String
                    let myImageURL = URL(string: (self.imagePath+image))
                    let myImageData = NSData(contentsOf: myImageURL!)
                    let screen = UIImage(data: myImageData! as Data)
                    arrayWithSomeScreens.append(screen!)
                }
                let myImageURL = URL(string: (self.imagePath+backImage))
                let myImageData = NSData(contentsOf: myImageURL!)
                let back_image = UIImage(data: myImageData! as Data)
                
                var runtime: Int? = nil
                if let runtime1 = readableJSON["runtime"] as? Int {
                    runtime = runtime1
                }
                var tag: String? = nil
                if let tag1 = readableJSON["tagline"] as? String{
                    tag = tag1
                }
                var voteCount: Int? = nil
                if let voteCount1 = readableJSON["vote_count"] as? Int {
                    voteCount = voteCount1
                }
                var voteAverage: Double? = nil
                if let voteAverage1 = readableJSON["vote_average"] as? Double {
                    voteAverage = voteAverage1
                }
                var releaseDate: String? = nil
                if let releaseDate1 = readableJSON["release_date"] as? String {
                    releaseDate = releaseDate1
                }
                var overview: String? = nil
                if let overview1 = readableJSON["overview"] as? String {
                    overview = overview1
                }
                
                print("  \(name)")
                let f = Film(name: name, backImage: back_image!, runtime: runtime!, tag: tag!, voteCount: voteCount!, voteAverage: voteAverage!, releaseDate: releaseDate!, overview: overview!,
                             arrayScreens: arrayWithSomeScreens)
                completion(f)

            } catch let error as NSError {
                print("JSON parsing error: \(error)")
            }
        }
        task.resume()
    }
    
}







