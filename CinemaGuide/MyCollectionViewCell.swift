//
//  MyCollectionViewCell.swift
//  CinemaGuide
//
//  Created by Gruszkovsky on 26.04.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var myImageView: UIImageView!
    
}
